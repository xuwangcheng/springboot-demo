package com.xuwangcheng.springboot.common.exception;

/**
 * 错误码
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/4/28 17:26
 */
public enum AppErrorCode {

    // 通用的
    SUCCESS_CODE(0, "OK"),
    SERVER_ERROR(9990, "系统内部错误"),
    USER_NOT_LOGIN(9991, "用户未登录"),
    FORBIDDEN(9992, "禁止访问"),
    INVALID_PARAMETER(9993, "参数校验失败[{}]"),
    DATABASE_EXCEPTION(9994, "数据库操作异常"),



    // 业务相关的错误码
    ;


    private int code;
    private String msg;

    AppErrorCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public static AppErrorCode getByCode(int code) {
        for (AppErrorCode c:values()) {
            if (code == c.getCode()) {
                return c;
            }
        }
        return null;
    }
}
