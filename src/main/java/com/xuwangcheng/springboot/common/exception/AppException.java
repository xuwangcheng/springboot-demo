package com.xuwangcheng.springboot.common.exception;

import cn.hutool.core.util.StrUtil;

/**
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/4/28 17:19
 */
public class AppException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private int code;
    private String msg;

    public AppException(AppErrorCode code) {
        this.code = code.getCode();
        this.msg = code.getMsg();
    }

    public AppException(AppErrorCode code, Object ...params) {
        this.code = code.getCode();
        this.msg = StrUtil.format(code.getMsg(), params);
    }

    public AppException(AppErrorCode code, Throwable e, Object ...params) {
        super(e);
        this.code = code.getCode();
        this.msg = StrUtil.format(code.getMsg(), params);
    }

    public AppException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public AppException(int code, String msg, Throwable e) {
        super(e);
        this.code = code;
        this.msg = msg;
    }


    public AppException(String msg) {
        super(msg);
        this.code = AppErrorCode.SERVER_ERROR.getCode();
        this.msg = msg;
    }

    public AppException(String msg, Throwable e) {
        super(msg, e);
        this.code = AppErrorCode.SERVER_ERROR.getCode();
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
