package com.xuwangcheng.springboot.common.config;

import com.xuwangcheng.springboot.common.utils.SpringContextUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 获取自定义配置属性
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/4/28 17:02
 */
@Data
@Component
public class CommonLocalSetting {

    @Value("${spring.profiles.active}")
    private String profiles;


    /**
     * 获取全局自定义设置
     * @author xuwangcheng
     * @date 2019/6/3 12:13
     * @param
     * @return {@link CommonLocalSetting}
     */
    public static CommonLocalSetting getCustomSetting () {
        return SpringContextUtils.getBean(CommonLocalSetting.class);
    }

}
