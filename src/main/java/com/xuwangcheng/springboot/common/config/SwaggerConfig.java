package com.xuwangcheng.springboot.common.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger配置
 * @author xuwangcheng
 * @date 2020-04-29
 */
@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class SwaggerConfig {

    @Value("${swagger.host}")
    private String swaggerHost;


    @Bean
    public Docket manage() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo("接口文档", "接口文档"))
                .host(this.swaggerHost)
                .select()
                .apis(Predicates.or(getSelector("com.xuwangcheng.springboot.modules")))
                .build()
                .groupName("系统管理")
                .directModelSubstitute(java.util.Date.class, String.class);
    }


    private Predicate<RequestHandler> getSelector(String packageName) {
        return RequestHandlerSelectors.basePackage(packageName);
    }

    private ApiInfo apiInfo(String name, String description) {
        return new ApiInfoBuilder()
                .title(name)
                .description(description)
                .termsOfServiceUrl("https://www.xuwangcheng.com")
                .version("1.0.0")
            .build();
    }

}