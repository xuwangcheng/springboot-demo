package com.xuwangcheng.springboot.common.base.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * 基础实体类，相关公共字段
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/4/29 9:25
 */
@Data
@ApiModel
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

}
