package com.xuwangcheng.springboot.common.base.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xuwangcheng.springboot.common.base.dto.BaseQueryInDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * BaseDao基类 T - Entity D -DTO
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/4/29 9:01
 */
public interface BaseDao<T, D> extends BaseMapper<T> {

    /**
     *  分页查询实体
     * @author xuwangcheng
     * @date 2020/4/29 9:05
     * @param page page
     * @param params params
     * @return {@link IPage}
     */
    IPage<D> selectPage(Page page, @Param("pm") BaseQueryInDTO params);

    /**
     *  查询全部的实体
     * @author xuwangcheng
     * @date 2020/4/29 9:06
     * @param params params
     * @return {@link List}
     */
    List<D> selectPage(@Param("pm") BaseQueryInDTO params);

}
