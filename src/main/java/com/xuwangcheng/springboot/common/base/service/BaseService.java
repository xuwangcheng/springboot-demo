package com.xuwangcheng.springboot.common.base.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.xuwangcheng.springboot.common.base.dto.BaseQueryInDTO;
import com.xuwangcheng.springboot.common.base.page.PageData;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 基础服务接口
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/4/29 9:08
 */
public interface BaseService<T, D> {

    boolean insert(T entity);

    boolean insertBatch(Collection<T> list);

    boolean insertBatch(Collection<T> list, int batchSize);

    boolean updateById(T entity);

    boolean update(T entity, Wrapper<T> updateWrapper);

    boolean updateBatchById(Collection<T> entityList);

    boolean updateBatchById(Collection<T> entityList, int batchSize);

    T selectById(Serializable id);

    boolean deleteById(Serializable id);

    boolean deleteBatchIds(Collection<? extends Serializable> idList);

    boolean deleteByWrapper(Wrapper wrapper);

    List<T> list(Wrapper wrapper);

    Integer selectCount(Wrapper wrapper);

    T selectOne(Wrapper<T> wrapper);

    List<Object> selectObjs(Wrapper<T> wrapper);

    PageData<D> queryDTOPage (BaseQueryInDTO params);

    List<D> listDTO (BaseQueryInDTO params);
}
