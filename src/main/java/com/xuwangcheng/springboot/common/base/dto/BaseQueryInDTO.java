package com.xuwangcheng.springboot.common.base.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 查询入参
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2019/5/29 16:44
 */
@ApiModel
@Data
public class BaseQueryInDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "当前页码，从1开始", dataType="int", example = "1")
    private Long page = 1L;
    @ApiModelProperty(value = "每页显示记录数", dataType="int", example = "10")
    private Long limit = 10L;
    @ApiModelProperty(value = "排序字段名")
    private String orderField;
    @ApiModelProperty(value = "排序方式，可选值(asc、desc)", example = "desc")
    private String order;
}
