package com.xuwangcheng.springboot.common.constant;

/**
 * 系统常量
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/4/29 9:21
 */
public interface SystemConstant {
    /**
     * 否
     */
    String FALSE = "0";
    /**
     * 是
     */
    String TRUE = "1";
    /**
     * 成功
     */
    int SUCCESS = 1;
    /**
     * 失败
     */
    int FAIL = 0;
    /**
     * OK
     */
    String OK = "OK";
    /**
     * 用户标识
     */
    String USER_KEY = "userId";

    /**
     *  升序
     */
    String ASC = "asc";
    /**
     * 降序
     */
    String DESC = "desc";
    /**
     * 创建时间字段名
     */
    String CREATE_DATE = "create_date";

    /**
     * 主键ID默认字段名
     */
    String ID = "id";

    /**
     * 当前页码
     */
    String PAGE = "page";
    /**
     * 每页显示记录数
     */
    String LIMIT = "limit";
    /**
     * 排序字段
     */
    String ORDER_FIELD = "orderField";
    /**
     * 排序方式
     */
    String ORDER = "order";
    /**
     * header
     */
    String TOKEN_HEADER = "token";

    /**
     * 通用文件下载content-type设置头
     */
    String COMMON_FILE_CONTENT_TYPE = "application/octet-stream";
}
