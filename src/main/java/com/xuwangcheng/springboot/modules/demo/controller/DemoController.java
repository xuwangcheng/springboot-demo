package com.xuwangcheng.springboot.modules.demo.controller;

import com.xuwangcheng.springboot.common.base.entity.Result;
import com.xuwangcheng.springboot.common.validate.ValidateUtil;
import com.xuwangcheng.springboot.modules.demo.entity.DemoEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/4/29 9:39
 */
@RestController
@RequestMapping("/demo")
@Api(tags = "demo程序")
public class DemoController {

    @PostMapping("test")
    @ApiOperation("测试")
    public Result<DemoEntity> test(@RequestBody DemoEntity entity) {
        ValidateUtil.validate(entity);
        return new Result<DemoEntity>().ok(entity);
    }
}
